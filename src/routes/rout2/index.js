import React from 'react';
import Layout from '../../components/Layout';
import Rout2 from './Rout2';
import fetch from '../../core/fetch';

export default {

  path: '/rout2',

  
  async action() {
    const resp = await fetch('http://api.openweathermap.org/data/2.5/weather?q=Moscow&appid=7fea172f95ba8bc0b875f6fe7774a0c8')
    .then(response => response.ok ? response : console.log(response.statusText))
    const data  = await resp.json();
    if (!data) throw new Error('Failed to load the Rout2 feed.') 
    const title = "Температура в "+data.name+" "+(Math.round((data.main.temp-273.5)*10)/10)+" Градусов Цельсия"

    return {
      title,
      component: <Layout><Rout2 title={title} /></Layout>,
    };
  },
};