import React from 'react';
import Layout from '../../components/Layout';
import fetch from '../../core/fetch';
import Rout1 from './Rout1';

/*let title = ''*/

export default {

  path: '/rout1',
  
  async action() {
    const resp = await fetch('http://api.openweathermap.org/data/2.5/weather?q=London&appid=7fea172f95ba8bc0b875f6fe7774a0c8')
    .then(response => response.ok ? response : console.log(response.statusText))
    const data  = await resp.json();
    if (!data) throw new Error('Failed to load the Rout1 feed.') 
    const title = "Температура в "+data.name+" "+(Math.round((data.main.temp-273.5)*10)/10)+" Градусов Цельсия"

    return {
      title,
      component: <Layout><Rout1 title={title} /></Layout>,
    };
  },
};